package com.sigeosrl.democfa.repository;

import com.sigeosrl.democfa.domain.Comune;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ComuneRepo extends JpaRepository<Comune,Long> {




}
