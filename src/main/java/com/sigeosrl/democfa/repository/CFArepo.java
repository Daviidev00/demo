package com.sigeosrl.democfa.repository;

import com.sigeosrl.democfa.domain.CFA;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CFArepo extends JpaRepository<CFA,Long> {
}
