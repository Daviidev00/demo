package com.sigeosrl.democfa.repository;

import com.sigeosrl.democfa.domain.Job;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobRepository extends JpaRepository<Job,Long> {
}
