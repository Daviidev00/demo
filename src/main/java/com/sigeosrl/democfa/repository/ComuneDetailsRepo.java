package com.sigeosrl.democfa.repository;

import com.sigeosrl.democfa.domain.Comune;
import com.sigeosrl.democfa.domain.ComuneDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ComuneDetailsRepo extends JpaRepository<ComuneDetails,Long> {

    public ComuneDetails findComuneDetailsByComuneId(Long id);




}
