package com.sigeosrl.democfa.repository;

import com.sigeosrl.democfa.domain.ECL;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ECLRepo extends JpaRepository<ECL,Long> {
}
