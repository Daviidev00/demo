package com.sigeosrl.democfa.repository;

import com.sigeosrl.democfa.domain.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task,Long> {


}
