package com.sigeosrl.democfa.controller;

import com.sigeosrl.democfa.domain.Job;
import com.sigeosrl.democfa.service.JobService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/job")
public class JobController {

    private final JobService jobService;


    public JobController(JobService jobService) {
        this.jobService = jobService;
    }

    @GetMapping("/id/{id}")
    public Job getJob(@PathVariable Long id){

        return jobService.getJob(id);
    }

    @PostMapping()
    public Job addJob(@RequestBody Job job){

        return jobService.addJob(job);
    }
}
