package com.sigeosrl.democfa.controller;


import com.sigeosrl.democfa.domain.Comune;
import com.sigeosrl.democfa.service.ComuneService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/comuni")
public class ComuneController {

    private final ComuneService comuneService;


    public ComuneController(ComuneService comuneService) {
        this.comuneService = comuneService;
    }

    @GetMapping("/id/{id}")
    public Comune getComune(@PathVariable Long id){

        return comuneService.getComune(id);
    }

    @GetMapping()
    public List<Comune> getAllComuni(){

        return comuneService.getAllComuni();
    }

    @PostMapping()
    public Comune addComune(@RequestBody Comune comune){

        return comuneService.addComune(comune);
    }

    @PutMapping("/id/{id}")
    public Comune updateComune(@PathVariable Long id, @RequestBody Comune comune){

        return comuneService.updateComune(comune,id);
    }

    @DeleteMapping("/id/{id}")
    public void deleteComune(@PathVariable Long id){

        comuneService.deleteComune(id);
    }
}
