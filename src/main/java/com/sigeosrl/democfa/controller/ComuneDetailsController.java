package com.sigeosrl.democfa.controller;


import com.sigeosrl.democfa.domain.CfaDto;
import com.sigeosrl.democfa.domain.ComuneDetails;
import com.sigeosrl.democfa.domain.ComuneDetailsDto;
import com.sigeosrl.democfa.domain.EclDto;
import com.sigeosrl.democfa.service.ComuneDetailsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/details")
public class ComuneDetailsController {


   private final ComuneDetailsService comuneDetailsService;


    public ComuneDetailsController(ComuneDetailsService comuneDetailsService) {
        this.comuneDetailsService = comuneDetailsService;
    }

    @GetMapping()
    public List<ComuneDetailsDto> getAllComuni(){

        return comuneDetailsService.getAllComuniDetails();
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<ComuneDetailsDto> getComuneDetails(@PathVariable Long id){

        return ResponseEntity.ok(comuneDetailsService.getComuneDetails(id));
    }

     @GetMapping("/cfa/comuneid/{id}")
     public Set<CfaDto> getCfaDto(@PathVariable Long id){

        return comuneDetailsService.getCfoDetails(id);
     }

     @GetMapping("/ecl/comuneid/{id}")
     public Set<EclDto> getEclDto(@PathVariable Long id){

        return comuneDetailsService.getEclDetails(id);
     }



    @PostMapping()
    public ComuneDetailsDto addComuneDetails(@RequestBody ComuneDetails comuneDetails){

        return comuneDetailsService.addComuneDetails(comuneDetails);
    }

    @PutMapping("/id/{id}")
    public ComuneDetailsDto updateComuneDetails(@PathVariable Long id,@RequestBody ComuneDetails comuneDetails){

        return comuneDetailsService.updateComuneDetails(comuneDetails,id);
    }

    @DeleteMapping("/id/{id}")
    public void deleteComuneDetails(@PathVariable Long id){

        comuneDetailsService.deleteComuneDetails(id);
    }

    @DeleteMapping()
    public void deleteAllComuneDetails(){

        comuneDetailsService.deleteAll();
    }



}
