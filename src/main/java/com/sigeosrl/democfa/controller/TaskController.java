package com.sigeosrl.democfa.controller;

import com.sigeosrl.democfa.domain.Task;
import com.sigeosrl.democfa.service.TaskService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/task")
public class TaskController {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/id/{id}")
    public Task getTask(@PathVariable Long id){

        return taskService.getTask(id);
    }

    @PostMapping()
    public Task addTask(@RequestBody Task task){

        return taskService.addTask(task);
    }
}
