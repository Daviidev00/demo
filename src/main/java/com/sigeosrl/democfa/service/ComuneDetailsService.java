package com.sigeosrl.democfa.service;


import com.sigeosrl.democfa.domain.CfaDto;
import com.sigeosrl.democfa.domain.ComuneDetails;
import com.sigeosrl.democfa.domain.ComuneDetailsDto;
import com.sigeosrl.democfa.domain.EclDto;
import com.sigeosrl.democfa.domain.mapper.ComuneDetailMapper;
import com.sigeosrl.democfa.repository.ComuneDetailsRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;


@Service
public class ComuneDetailsService {

    private final ComuneDetailsRepo comuneDetailsRepo;

    private final ComuneDetailMapper comuneDetailMapper;




    public ComuneDetailsService(ComuneDetailsRepo comuneDetailsRepo, ComuneDetailMapper comuneDetailMapper) {
        this.comuneDetailsRepo = comuneDetailsRepo;
        this.comuneDetailMapper = comuneDetailMapper;
    }

    public List<ComuneDetailsDto> getAllComuniDetails(){

        return comuneDetailMapper.toDto(comuneDetailsRepo.findAll());
    }

    public ComuneDetailsDto getComuneDetails(Long id){
        ComuneDetails comuneDetails = comuneDetailsRepo.findById(id).get();

        return comuneDetailMapper.toDto(comuneDetails);
    }

    public Set<CfaDto> getCfoDetails(Long id){

        ComuneDetailsDto comuneDetails =  comuneDetailMapper.toDto(comuneDetailsRepo.findComuneDetailsByComuneId(id));

        return comuneDetails.getCfaList();
    }

    public Set<EclDto> getEclDetails(Long id){

        ComuneDetailsDto comuneDetails =  comuneDetailMapper.toDto(comuneDetailsRepo.findComuneDetailsByComuneId(id));

        return comuneDetails.getEclList();
    }

    public ComuneDetailsDto addComuneDetails(ComuneDetails comuneDetails){

        return comuneDetailMapper.toDto(comuneDetailsRepo.save(comuneDetails));
    }

    public ComuneDetailsDto updateComuneDetails(ComuneDetails comuneDetails, Long id){

        ComuneDetails newComuneDetails = comuneDetailsRepo.findById(id).get();

        newComuneDetails.setComune(comuneDetails.getComune());
        newComuneDetails.setCfaList(comuneDetails.getCfaList());
        newComuneDetails.setEclList(comuneDetails.getEclList());

        comuneDetailsRepo.deleteById(id);
        return   comuneDetailMapper.toDto(comuneDetailsRepo.save(newComuneDetails));
    }

    public void deleteComuneDetails(Long id){

        comuneDetailsRepo.deleteById(id);

    }

    public void deleteAll(){

        comuneDetailsRepo.deleteAll();
    }


}
