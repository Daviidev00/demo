package com.sigeosrl.democfa.service;

import com.sigeosrl.democfa.domain.Task;
import com.sigeosrl.democfa.repository.TaskRepository;
import org.springframework.stereotype.Service;

@Service
public class TaskService {

    private final TaskRepository taskRepository;


    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task getTask(Long id){

        return taskRepository.findById(id).get();
    }

    public Task addTask(Task task){

        return taskRepository.save(task);
    }
}
