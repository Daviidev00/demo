package com.sigeosrl.democfa.service;


import com.sigeosrl.democfa.domain.Comune;
import com.sigeosrl.democfa.repository.ComuneRepo;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class ComuneService {

    private final ComuneRepo comuneRepo;

    public ComuneService(ComuneRepo comuneRepo) {
        this.comuneRepo = comuneRepo;
    }

    public Comune getComune(Long id){

        return comuneRepo.findById(id).get();
    }

    public List<Comune> getAllComuni(){



        return comuneRepo.findAll();
    }

    public Comune updateComune(Comune comune, Long id){

        Comune newComune = comuneRepo.findById(id).get();

        newComune.setName(comune.getName());
        comuneRepo.deleteById(id);
        return comuneRepo.save(newComune);
    }

    public Comune addComune(Comune comune){

        return comuneRepo.save(comune);
    }

    public void deleteComune(Long id){

        comuneRepo.deleteById(id);
    }


}
