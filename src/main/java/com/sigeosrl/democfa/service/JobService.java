package com.sigeosrl.democfa.service;


import com.sigeosrl.democfa.domain.Job;
import com.sigeosrl.democfa.repository.JobRepository;
import org.springframework.stereotype.Service;

@Service
public class JobService {

    private final JobRepository jobRepository;


    public JobService(JobRepository jobRepository) {
        this.jobRepository = jobRepository;
    }

    public Job getJob(Long id){

        return jobRepository.findById(id).get();
    }

    public Job addJob(Job job){

        return jobRepository.save(job);
    }
}
