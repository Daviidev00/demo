package com.sigeosrl.democfa.domain;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Set;

@Data
@Builder
@Accessors(chain = true)
public class ComuneDetailsDto {

    private Long id;
    private Set<EclDto> eclList;
    private Set<CfaDto> cfaList;
    private Comune comune;
}
