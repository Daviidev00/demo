package com.sigeosrl.democfa.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Set;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ComuneDetails {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne
    private Comune comune;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private Set<CfaDto> cfaList;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private Set<EclDto> eclList;


}
