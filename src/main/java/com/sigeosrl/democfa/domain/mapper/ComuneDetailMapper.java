package com.sigeosrl.democfa.domain.mapper;

import com.sigeosrl.democfa.domain.ComuneDetails;
import com.sigeosrl.democfa.domain.ComuneDetailsDto;
import com.sigeosrl.democfa.repository.ComuneDetailsRepo;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ComuneDetailMapper {

    public ComuneDetailsDto toDto(ComuneDetails comuneDetails){

        return ComuneDetailsDto.builder()
                .id(comuneDetails.getId())
                .comune(comuneDetails.getComune())
                .cfaList(comuneDetails.getCfaList())
                .eclList(comuneDetails.getEclList())
                .build();
    }

    public List<ComuneDetailsDto> toDto(List<ComuneDetails> comuneDetails){

        return comuneDetails.stream().map(comuneDetails1 -> toDto(comuneDetails1)).collect(Collectors.toList());
    }

    public ComuneDetails toEntity(ComuneDetailsDto comuneDetailsDto){

        ComuneDetails comuneDetails = new ComuneDetails();

        comuneDetails.setComune(comuneDetailsDto.getComune());
        comuneDetails.setCfaList(comuneDetailsDto.getCfaList());
        comuneDetails.setEclList(comuneDetailsDto.getEclList());
        comuneDetails.setId(comuneDetails.getId());

        return comuneDetails;

    }
}
