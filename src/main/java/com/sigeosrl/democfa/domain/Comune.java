package com.sigeosrl.democfa.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "comune")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Comune {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;




}