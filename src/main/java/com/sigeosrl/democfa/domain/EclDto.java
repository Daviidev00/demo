package com.sigeosrl.democfa.domain;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class EclDto {


    private Status status;
    private int quantity;
}
