package com.sigeosrl.democfa.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Access;
import javax.persistence.Id;

@Data
@Accessors(chain = true)
public class CfaDto {

    private Status status;

    private int quantity;
}
