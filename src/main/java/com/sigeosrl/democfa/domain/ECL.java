package com.sigeosrl.democfa.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ECL {

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO)
    private Long id;


    @ManyToOne
    @JoinColumn(name = "comune_ID")
    private Comune comune;


    private String des;

    private String status;
}
