package com.sigeosrl.democfa.domain;

public enum Status {


    VOID,
    CREATED,    // CREATA
    WIP,
    SKIPPED,
    DOWNGRADED, // DECLASSATA
    UPGRADED,
    DONE,
    OK,
    DELETED,
    TO_CHECK_ON_SITE, // DA RILEVARE
    VERIFIED_ON_SITE, // RILEVATA
    REPORTED,   // SEGNALATA
    CONFIRMED   // CONFERMATA
}
