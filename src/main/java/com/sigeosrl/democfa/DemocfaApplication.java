package com.sigeosrl.democfa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemocfaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemocfaApplication.class, args);
	}

}
